/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package factorial;

/**
 *
 * @author richard.wasniowski
 */
public class Factorial {

       public static void main(String[] args) {
      int n = 19;
      int factorial = 1;
 
      // n! = 1*2*3...*n
      for (int i = 1; i <= n; i++) {
         factorial *= i;
      }
      System.out.println("The Factorial of " + n + " is " + factorial);
   }
}
